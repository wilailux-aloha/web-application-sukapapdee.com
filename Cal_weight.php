<meta charset="utf-8">
<?php session_start(); ?>
<?php
  if (!$_SESSION["UserID"]){  //check session
    echo "<script>";
    echo "alert(\" ขอสงวนสิทธฺิ์การใช้งานหน้านี้สำหรับสมาชิก! กรุณาเข้าสู่ระบบ หรือสมัครสมาชิกก่อนค่ะ \");";
    echo "window.location=\"login.php\";";
    echo "</script>";
  }else {?>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>DayCalorie</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:700,400">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/elegant-font/code/style.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <link rel="stylesheet" href="assets/css/magnific-popup.css">
        <link rel="stylesheet" href="assets/flexslider/flexslider.css">
        <link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/media-queries.css">
        <!-- <link rel="stylesheet" href="assets/css/style_from.css"> -->

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    <!-- Theme CSS -->
    <link href="assets/css/agency.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <style>
    #result-panel_m {
      display:none;
    }
    #result-panel_f {
      display:none;
    }


    .th {
      background: #ff6a33;
      color: #fff;
      font-size: 15px;
      font-family: 'Montserrat', sans-serif;
    } .lady{
      background: #ff7590;
      color: #fff;
    }.man{
      background: #6e81db;
      color: #fff;
    }
    table{
      font-size: 14px;
      font-family: 'Montserrat', sans-serif;
      border: solid 1px #c3c3c3;
      width: 100%;
    }
    </style>
    </head>

    <body>

      <!-- Top menu -->
  <nav class="navbar" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
          <span class="sr-only"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.html"></a>
      </div><br>
      <!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="top-navbar-1">
          <ul class="nav navbar-nav navbar-right">
						<li>
							<a href="index.php"><span aria-hidden="true" class="icon_house"></span><br>หน้าแรก</a>
						</li>
						<li>
							<a href="Cal_food.php"><span aria-hidden="true" class="glyphicon glyphicon-th-list"></span><br>ตารางแคล</a>
						</li>
						<li >
							<a href="info.php"><span aria-hidden="true" class="glyphicon glyphicon-bullhorn"></span><br>สาระสุขภาพ</a>
						</li>
						<li class="active">
							<a href="Cal.php"><span aria-hidden="true" class="glyphicon glyphicon-calendar"></span><br>โปรแกรมคำนวณ</a>
						</li>

						<li>
							<a href="register.php"><span aria-hidden="true" class="glyphicon glyphicon-edit"></span><br>สมัครสมาชิก</a>
						</li>

					</ul>
				</div>
			</div>
		</nav>

    <!-- Page Title -->
    <div class="page-title-container">
        <div class="container">
              <div class="row">
                    <div class="col-sm-10 col-sm-offset-1  wow fadeIn">
                        <h3><center>ยินดีต้อนรับคุณ <?= $_SESSION['user_name']?> ^ ^</center></h3>
                        <!-- <?php print_r($_SESSION);?> -->

                </div>

                  </div>
              </div>
            </div><!--  -->

            <div class="container">
                  <div class="row">
                        <div class="col-sm-9 col-sm-offset-7">
                          <a href="EditRecord.php"><img src ="./assets/img/icon/edit.png">แก้ไขข้อมูล</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          <a href="logout.php" onclick="out()"><img src ="./assets/img/icon/u3.png">ออกจากระบบ</a>

                    </div>

                      </div>
                  </div>
                  <script type="text/javascript">
                  function out(){
                    alert("แน่ใจว่าคุณต้องการออกจากระบบ?")
                  }
                  </script>


                <div class="col-sm-10 col-sm-offset-1  wow fadeIn">
                  <div class="container">
                      <div class="row">
                  <div class="col-md-3">
                      <div class="panel panel-default panel-box">
                          <a href="#" class="btn btn-lg btn-danger btn-block">โปรแกรมคำนวนร่างกาย</a>
                          <div style="height: 25px;"></div>
                          <div class="btn-group-vertical">
                            <ul class="mail-ul nav">
                              <li><a href="Cal.php" class="list-group-item list-group-item-action list-group-item-warning">ดัชนีมวลกาย(BMI)</a></li>
                              <div style="height: 10px;"></div>
                               <li><a href="Cal_bmr.php" class="list-group-item list-group-item-action list-group-item-warning">อัตราการเผาผลาญพลังงาน(BMR)</a></li>
                               <div style="height: 10px;"></div>
                                <li><a href="Cal_day.php" class="list-group-item list-group-item-action list-group-item-warning">แคลอรี่ที่เหมาะสมต่อวัน</a></li>
                                <div style="height: 10px;"></div>
                                 <li><a href="Cal_weight.php" class="list-group-item list-group-item-action list-group-item">คำนวณน้ำหนักที่เหมาะสม</a></li>
                                 <div style="height: 25px;"></div>
                          </ul>
                          </div>
                      </div>
                  </div>
                  <div class="panel panel-defaul col-md-9">
                    <div class="panel-box">
                    <center><h4>Day Calorie Calculator</h4><a>** ค่ามาตรฐานน้ำหนักและส่วนสูงของคนไทย **</a></center><br><br>

                          <form id="myForm">
                              <div class="form-group">
                                <label for="lengte2" class="col-sm-2 control-label">ส่วนสูง (cm.)</label>
                                <div class="col-sm-8">
                                    <input type = "number" class = "form-control" id = "lengte2">

                                </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              </div><br>

                              <label for = "man"></label>เพศ : &nbsp;&nbsp;&nbsp;&nbsp;
                              <input type = "radio" name ="geslacht" id = "man" value = "man"/>ชาย
                              <label for = "Lady"></label>&nbsp;&nbsp;
                              <input type = "radio" name ="geslacht" id = "Lady" value = "Lady"/>หญิง
                              <p></p><br>

                              <button class = "btn btn-warning" type = "button" id = "button2">ยืนยัน</button>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              <button class = "btn btn-warning" type = "button" onclick="myFunction()">รีเซ็ต</button>
                              <br><br>
                            </form>

                    </div>
                    <div class="col-md-6 col-md-offset-3">

                   <table class="table table-hover" id="result-panel_m">

                     <!-- <h5>ค่ามาตรฐานน้ำหนักและส่วนสูงของคนไทย</h5> -->
                    <thead>
                    <tr><th colspan="2" class="man"><center><h5><img src ="./assets/img/icon/m.png"> เพศชาย</h5></center></th></tr>
                    <tr><th class="man"><center><h5>ส่วนสูง (ซม.)</h5></center></th>
                    <th class="man" ><center><h5>น้ำหนัก (กก.)</h5></center></th>
                    </tr>
                    </thead>
                    <tbody>
                   <tr><td align="center"><p id="result_v"></p></td><td align="center"><p id="result"></p></td></tr>
                    </tbody>
                    </table>
                    </div>

                    <div class="col-md-6 col-md-offset-3">
                    <table class="table table-hover" id="result-panel_f">

                     <thead>
                     <tr>
                     <th colspan="2" class="lady"><center><h5><img src ="./assets/img/icon/fm.png"> เพศหญิง</h5></center></th>
                     </tr>
                     <tr>
                     <th class="lady"><center><h5>ส่วนสูง (ซม.)</h5></center></th>
                     <th class="lady"><center>
                       <h5>น้ำหนัก (กก.)</h5></center></th>
                     </tr>
                     </thead>
                     <tbody>
                       <tr><td align="center"><p id="result_vf"></p></td><td align="center"><p id="result_f"></p></td></tr>

                     </tbody>
                     </table>
                     </div>



                      </div>
                    </div>
                  </div>
          </div>

          <script>
          $(document).ready(
            $("#button2").on("click",chek)
          );

          function myFunction() {
              document.getElementById("myForm").reset();
          }

          function chek() {
            var length = $("#lengte2").val();
            var gender = $("input[name='geslacht']:checked").val();

            if (length<100 || length>200) {
                alert ("กรุณาใส่น้ําหนักให้มีค่าระหว่าง 100 – 200 kg!");
            }else if(gender !="man" && gender !="Lady"){
                alert ("กรุณาเลือกเพศ");
            }else {
              Weight();
            }
          }

          function Weight(){
          var length = $("#lengte2").val();
          var gender = $("input[name='geslacht']:checked").val();
          var WeightCalculation = 0;

          if (gender == "man"){
            WeightCalculation = (length-100);
            $("#result-panel_m").fadeIn(3000);
            document.getElementById('result').innerHTML = WeightCalculation.toFixed(1);
            document.getElementById('result_v').innerHTML = length;
          }
          else{
            WeightCalculation = (length-100)*0.9;
            $("#result-panel_f").fadeIn(3000);
            document.getElementById('result_f').innerHTML = WeightCalculation.toFixed(1);
            document.getElementById('result_vf').innerHTML = length;
          }
          }
          </script>






        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/bootstrap-hover-dropdown.min.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/retina-1.1.0.min.js"></script>
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <script src="assets/flexslider/jquery.flexslider-min.js"></script>
        <script src="assets/js/jflickrfeed.min.js"></script>
        <script src="assets/js/masonry.pkgd.min.js"></script>
        <script src="assets/js/jquery.ui.map.min.js"></script>
        <script src="assets/js/scripts.js"></script>

    </body>

</html>
<?php }?>
