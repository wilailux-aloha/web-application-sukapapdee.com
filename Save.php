<meta charset="utf-8">
<?php session_start(); ?>
<?php
  if (!$_SESSION["UserID"]){  //check session
    echo "<script>";
    echo "alert(\" ขอสงวนสิทธฺิ์การใช้งานหน้านี้สำหรับสมาชิก! กรุณาเข้าสู่ระบบ หรือสมัครสมาชิกก่อนค่ะ \");";
    echo "window.location=\"login.php\";";
    echo "</script>";
  }else {?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>คำนวณร่างกาย</title>
        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:700,400">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/elegant-font/code/style.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <link rel="stylesheet" href="assets/css/magnific-popup.css">
        <link rel="stylesheet" href="assets/flexslider/flexslider.css">
        <link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/media-queries.css">
        <!-- <link rel="stylesheet" href="assets/css/style_from.css"> -->
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    <!-- Theme CSS -->
    <link href="assets/css/agency.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <style>
    #result-panel {
      /*visibility: hidden;*/
      display:none;
    }
    #result {
      font-size: 20px;
      font-family: 'Montserrat', sans-serif;
      text-align:center;
    }
    th {
      background: #ff6a33;
      color: #fff;
      font-size: 15px;
      font-family: 'Montserrat', sans-serif;
    }
    tr.bmi td {
      background: #f9d336;
      color: #fff;
    }
    table{
      font-size: 15px;
      font-family: 'Montserrat', sans-serif;
      border: solid 1px #c3c3c3;

    }
    .flip {
    padding: 20px;
    text-align: center;
    background-color: #ffe5b4;
    border: solid 1px #c3c3c3;
    }
    </style>
    </head>

    <body>

      <!-- Top menu -->
  <nav class="navbar" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.html"></a>
      </div><br>
      <!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="top-navbar-1">
          <ul class="nav navbar-nav navbar-right">
						<li>
							<a href="index.php"><span aria-hidden="true" class="icon_house"></span><br>หน้าแรก</a>
						</li>
						<li>
							<a href="Cal_food.php"><span aria-hidden="true" class="glyphicon glyphicon-th-list"></span><br>ตารางแคล</a>
						</li>
						<li >
							<a href="info.php"><span aria-hidden="true" class="glyphicon glyphicon-bullhorn"></span><br>สาระสุขภาพ</a>
						</li>
						<li class="active">
							<a href="Cal.php"><span aria-hidden="true" class="glyphicon glyphicon-calendar"></span><br>โปรแกรมคำนวณ</a>
						</li>
						
						<li>
							<a href="register.php"><span aria-hidden="true" class="glyphicon glyphicon-edit"></span><br>สมัครสมาชิก</a>
						</li>

					</ul>
				</div>
			</div>
		</nav>

        <!-- Page Title -->
        <div class="page-title-container">
            <div class="container">
                  <div class="row">
                        <div class="col-sm-10 col-sm-offset-1  wow fadeIn">
                            <h3><center>ยินดีต้อนรับคุณ <?= $_SESSION['name']?> ^ ^</center></h3>
                            <!-- <?php print_r($_SESSION);?> -->

                    </div><br><br><br>

                      </div>
                  </div>
                </div><!--  -->






                <div class="col-sm-10 col-sm-offset-2  wow fadeIn" border="3">
                  <div class="container">
                      <div class="row">

                  <div class="panel panel-defaul col-sm-9">
                    <div class="panel-box">
                  <br>

                    <!-- <form id="myForm">
                      <div class="col-md-5 ">
                                  <label for="gewicht2"class="col-sm-5 control-label">ชื่อ</label>
                                  <input type = "text" class = "form-control" id = "InputWeight">
                                  <p></p>
                                  <label for = "lengte2" class="col-sm-5 control-label">รหัสผ่าน</label>
                                  <input type = "text" class = "form-control" id = "InputHeight">
                                  <p></p>
                                  <label for = "lengte2" class="col-sm-5 control-label">อีเมล</label>
                                  <input type = "text" class = "form-control" id = "InputHeight">
                                  <p></p><br>
                                  <button class = "btn btn-warning" type = "button" >ยืนยัน</button>
                                  <br><br><br>
                             </div>


                                </form> -->
                                <?php
                include "config.php";
                $objConnect = mysql_connect("$servername","$username","$password") or die("Error Connect to Database");
                $objDB = mysql_select_db("$dbname");
                $ID = $_SESSION['UserID'];
                $strSQL = "UPDATE tb_user SET ";
                $strSQL .="user_name = '$_POST[name]' ";
                $strSQL .=",password = '$_POST[Pass]' ";
                $strSQL .=",email = '$_POST[Email]' ";
                $strSQL .="WHERE userID = '$ID' ";

                $objQuery = mysql_query($strSQL);
                if($objQuery)
                {
                	echo "บันทึกข้อมูลสำเสร็จแล้วคะ.";
                  echo "<br>";
                  echo "<img src ='./assets/img/icon/cc.png'>";
                  echo "<br>.<br>";
                  echo "<a href='Cal.php'><button class = 'btn btn-warning' type = 'submit' name='submit'>กลับไปหน้าคำนวณ</button></a>";
                 echo "<br>.<br>";
                }
                else
                {
                	echo "Error Save [".$strSQL."]".  $objDB->error;
                }
                mysql_close($objConnect);
        ?>


                    </div>
                      </div>



                    </div>
                  </div>
          </div>









        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/bootstrap-hover-dropdown.min.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/retina-1.1.0.min.js"></script>
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <script src="assets/flexslider/jquery.flexslider-min.js"></script>
        <script src="assets/js/jflickrfeed.min.js"></script>
        <script src="assets/js/masonry.pkgd.min.js"></script>

        <script src="assets/js/jquery.ui.map.min.js"></script>
        <script src="assets/js/scripts.js"></script>

    </body>

</html>
<?php }?>
