<meta charset="utf-8">
<?php session_start(); ?>
<?php
  if (!$_SESSION["UserID"]){  //check session
    echo "<script>";
    echo "alert(\" ขอสงวนสิทธฺิ์การใช้งานหน้านี้สำหรับสมาชิก! กรุณาเข้าสู่ระบบ หรือสมัครสมาชิกก่อนค่ะ \");";
    echo "window.location=\"login.php\";";
    echo "</script>";
  }else {?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>คำนวณร่างกาย</title>
        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:700,400">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/elegant-font/code/style.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <link rel="stylesheet" href="assets/css/magnific-popup.css">
        <link rel="stylesheet" href="assets/flexslider/flexslider.css">
        <link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/media-queries.css">
        <!-- <link rel="stylesheet" href="assets/css/style_from.css"> -->
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    <!-- Theme CSS -->
    <link href="assets/css/agency.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <style>
    #result-panel {
      /*visibility: hidden;*/
      display:none;
    }
    #result {
      font-size: 20px;
      font-family: 'Montserrat', sans-serif;
      text-align:center;
    }
    th {
      background: #ff6a33;
      color: #fff;
      font-size: 15px;
      font-family: 'Montserrat', sans-serif;
    }
    tr.bmi td {
      background: #f9d336;
      color: #fff;
    }
    table{
      font-size: 15px;
      font-family: 'Montserrat', sans-serif;
      border: solid 1px #c3c3c3;

    }
    .flip {
    padding: 20px;
    text-align: center;
    background-color: #ffe5b4;
    border: solid 1px #c3c3c3;
    }
    </style>
    </head>

    <body>

      <!-- Top menu -->
  <nav class="navbar" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.html"></a>
      </div><br>
      <!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="top-navbar-1">
          <ul class="nav navbar-nav navbar-right">
						<li>
							<a href="index.php"><span aria-hidden="true" class="icon_house"></span><br>หน้าแรก</a>
						</li>
						<li>
							<a href="Cal_food.php"><span aria-hidden="true" class="glyphicon glyphicon-th-list"></span><br>ตารางแคล</a>
						</li>
						<li >
							<a href="info.php"><span aria-hidden="true" class="glyphicon glyphicon-bullhorn"></span><br>สาระสุขภาพ</a>
						</li>
						<li class="active">
							<a href="Cal.php"><span aria-hidden="true" class="glyphicon glyphicon-calendar"></span><br>โปรแกรมคำนวณ</a>
						</li>

						<li>
							<a href="register.php"><span aria-hidden="true" class="glyphicon glyphicon-edit"></span><br>สมัครสมาชิก</a>
						</li>

					</ul>
				</div>
			</div>
		</nav>

        <!-- Page Title -->
        <div class="page-title-container">
            <div class="container">
                  <div class="row">
                        <div class="col-sm-10 col-sm-offset-1  wow fadeIn">
                            <h3><center>ยินดีต้อนรับคุณ <?= $_SESSION['user_name']?> ^ ^</center></h3>
                            <!-- <?php print_r($_SESSION);?> -->

                    </div>

                      </div>
                  </div>
                </div><!--  -->


                    <div class="container">
                          <div class="row">
                                <div class="col-md-9 col-md-offset-6">
                                    <a href="EditRecord.php"><img src ="./assets/img/icon/edit.png">แก้ไขข้อมูล</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <a href="logout.php" onclick="out()"><img src ="./assets/img/icon/u3.png">ออกจากระบบ</a>

                            </div>

                              </div>
                          </div>
                          <script type="text/javascript">
                          function out(){
                          	alert("แน่ใจว่าคุณต้องการออกจากระบบ?")
                          }
                          </script>



                <div class="col-sm-10 col-sm-offset-1  wow fadeIn">
                  <div class="container">
                      <div class="row">
                  <div class="col-sm-3">
                      <div class="panel panel-default panel-box">
                          <a href="#" class="btn btn-lg btn-danger btn-block">โปรแกรมคำนวนร่างกาย</a>
                          <div style="height: 25px;"></div>
                          <div class="btn-group-vertical">
                            <ul class="mail-ul nav">
                              <li class="active"><a href="#" class="list-group-item list-group-item-action list-group-item">ดัชนีมวลกาย(BMI)</a></li>
                              <div style="height: 10px;"></div>
                               <li><a href="Cal_bmr.php" class="list-group-item list-group-item-action list-group-item-warning">อัตราการเผาผลาญพลังงาน(BMR)</a></li>
                               <div style="height: 10px;"></div>
                                <li><a href="Cal_day.php" class="list-group-item list-group-item-action list-group-item-warning ">แคลอรี่ที่เหมาะสมต่อวัน</a></li>
                                <div style="height: 10px;"></div>
                                 <li><a href="Cal_weight.php" class="list-group-item list-group-item-action list-group-item-warning">คำนวณน้ำหนักที่เหมาะสม</a></li>
                                 <div style="height: 25px;"></div>
                          </ul>
                          </div>
                      </div>
                  </div>
                  <div class="panel panel-defaul col-sm-9">
                    <div class="panel-box">
                    <center><h4>BMI Calculator</h4><a>** คำนวณหาดัชนีมวลกาย **</a></center><br>

                    <form id="myForm">
                      <div class="col-md-5 ">
                                  <label for="gewicht2"class="col-sm-5 control-label">น้ำหนัก (kg.)</label>
                                  <input type = "number" class = "form-control" id = "InputWeight">
                                  <p></p>
                                  <label for = "lengte2" class="col-sm-5 control-label">ส่วนสูง(cm.)</label>
                                  <input type = "number" class = "form-control" id = "InputHeight">
                                  <p></p><br>
                                  <button class = "btn btn-warning" type = "button" onclick="chek()">ยืนยัน</button> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                  <button class = "btn btn-warning" type = "button" onclick="myFunction()">รีเซ็ต</button><br><br><br>
                             </div>
                                  <div class="col-sm-7" id="result-panel">

                                    <table class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline" >
                                      <tbody>
                                        <tr class="" id="tr0">
                                          <th bgcolor="#fedc8b"><strong>ค่า BMI</strong></th>
                                          <th bgcolor="#fedc8b"><strong>อยู่ในเกณท์</strong></th>
                                          <th bgcolor="#fedc8b"><strong>ภาวะเสี่ยงต่อโรค</strong></th>
                                        </tr>
                                        <tr class="" id="tr1">
                                          <td>น้อยกว่า 18.50</td>
                                          <td>น้ำหนักน้อย / ผอม</td>
                                          <td>มากกว่าคนปกติ</td>
                                        </tr>
                                        <tr class="" id="tr2">
                                          <td>ระหว่าง 18.50 - 22.90</td>
                                          <td>ปกติ (สุขภาพดี)</td>
                                          <td>เท่าคนปกติ</td>
                                        </tr>
                                        <tr class="" id="tr3">
                                          <td>ระหว่าง 23 - 24.90</td>
                                          <td>ท้วม / โรคอ้วนระดับ 1</td>
                                          <td>อันตรายระดับ 1</td>
                                        </tr>
                                        <tr class="" id="tr4">
                                          <td>ระหว่าง 25 - 29.90</td>
                                          <td>อ้วน / โรคอ้วนระดับ 2</td>
                                          <td>อันตรายระดับ 2</td>
                                        </tr>
                                        <tr class="" id="tr5">
                                          <td>มากกว่า 30</td>
                                          <td>อ้วนมาก / โรคอ้วนระดับ 3</td>
                                          <td>อันตรายระดับ 3</td>
                                        </tr>
                                      </tbody>
                                    </table><br>
                                  </div>

                                </form>

                    </div>
                      </div>



                    </div>
                  </div>
          </div>



      <script>
      function chek(){
        var h = document.getElementById("InputHeight").value;
        var w = document.getElementById("InputWeight").value;
        if (w<30||w>200){
          alert ("กรุณาใส่น้ําหนักให้มีค่าระหว่าง 30 – 200 kg!")
        }else if (h<100||h>250){
          alert ("กรุณาใส่ส่วนสูงให้มีค่าระหว่าง 100 – 250 cm !")
        }else {
          Bmi();
        }
      }
      function myFunction() {
          document.getElementById("myForm").reset();
      }
      function Bmi() {
        var h=(document.getElementById("InputHeight").value)/100;
      	var w=document.getElementById("InputWeight").value;
      	var sum = w/(h*h);
        // var resultShow = document.getElementById("result-panel");
      	// resultShow.style.visibility="visible";
        $("#result-panel").slideDown(1000);

      	if(sum.toFixed(2)<18.50){
      		var show=document.getElementById("tr1");
      		show.setAttribute("class","bmi");
      	}else if(sum.toFixed(2)<22.90){
      		var show=document.getElementById("tr2");
      		show.setAttribute("class","bmi");
      	}else if(sum.toFixed(2)<24.90){
      		var show=document.getElementById("tr3");
      		show.setAttribute("class","bmi");
      	}else if(sum.toFixed(2)<29.90){
      		var show=document.getElementById("tr4");
      		show.setAttribute("class","bmi");
      	}else {
      		var show=document.getElementById("tr5");
      		show.setAttribute("class","bmi");
      	}
      document.getElementById("result").innerHTML=sum.toFixed(2);
      }
      </script>






        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/bootstrap-hover-dropdown.min.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/retina-1.1.0.min.js"></script>
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <script src="assets/flexslider/jquery.flexslider-min.js"></script>
        <script src="assets/js/jflickrfeed.min.js"></script>
        <script src="assets/js/masonry.pkgd.min.js"></script>

        <script src="assets/js/jquery.ui.map.min.js"></script>
        <script src="assets/js/scripts.js"></script>

    </body>

</html>
<?php }?>
