<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Sukapapdee.com</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:700,400">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/elegant-font/code/style.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <link rel="stylesheet" href="assets/css/magnific-popup.css">
        <link rel="stylesheet" href="assets/flexslider/flexslider.css">
        <link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/media-queries.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body>

        <!-- Top menu -->
		<nav class="navbar" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.html"></a>
				</div><br>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="top-navbar-1">
					<ul class="nav navbar-nav navbar-right">
						<li class="active">
							<a href="index.html"><span aria-hidden="true" class="icon_house"></span><br>หน้าแรก</a>
						</li>
						<li>
							<a  href="Cal_food.php"><span aria-hidden="true" class="glyphicon glyphicon-th-list"></span><br>ตารางแคล</a>
						</li>
						<li>
							<a href="info.php"><span aria-hidden="true" class="glyphicon glyphicon-bullhorn"></span><br>สาระสุขภาพ</a>
						</li>
						<li>
							<a href="Cal.php"><span aria-hidden="true" class="glyphicon glyphicon-calendar"></span><br>โปรแกรมคำนวณ</a>
						</li>
						<li>
							<a class="region-inner"  href="login.php"><span aria-hidden="true" class="icon_profile"></span><br>เข้าสู่ระบบ</a>
						</li>
						<li>
							<a href="register.php"><span aria-hidden="true" class="glyphicon glyphicon-edit"></span><br>สมัครสมาชิก</a>
						</li>


					</ul>
				</div>
			</div>
		</nav>

        <!-- Slider -->
        <div class="slider-container">
            <div class="slider">
                <div class="flexslider">
                    <ul class="slides">
                      <li>
                          <a href="info.php"><img src="./assets/img/slider/7.jpg"></a>

                      </li>
                      <li>
                          <a href="info.php"><img src="./assets/img/slider/4.jpg"></a>

                      </li>
                        <li>
                            <a href="info.php"><img src="./assets/img/slider/5.jpg"></a>

                        </li>
                        <li>
                          <a href="info.php">  <img src="./assets/img/slider/10.jpg"></a>

                        </li>
                        <li>
                            <a href="info.php"><img src="./assets/img/slider/9.jpg"></a>

                        </li>

                    </ul>
                </div>
            </div>
        </div>

        <!-- Presentation -->
        <div class="presentation-container">
        	<div class="container">
        		<div class="row">
	        		<div class="col-sm-12 wow fadeInLeftBig">
	            		<h1>ความรู้เกี่ยวกับการดูแลสุขภาพ</h1>

	            	</div>
            	</div>
        	</div>
        </div>

        <!-- Services -->
        <div class="services-container">
	        <div class="container">
	            <div class="row">
	            	<div class="col-sm-3">
		                <div class="service wow fadeInUp">
		                    <div class="service-icon"><span><img src="./assets/img/icon/k.png" width="64px" height="64px"></span></div>
		                    <h3>ความรู้ทั่วไป</h3>
		                    <a class="big-link-1" href="info.php">อ่าน</a>
		                </div>
					</div>
					<div class="col-sm-3">
		                <div class="service wow fadeInDown">
		                    <div class="service-icon"><span><img src="./assets/img/icon/f.png" width="70px" height="70px"></span></div>
		                    <h3>อาหารการกิน</h3>
                        <a class="big-link-1" href="info.php">อ่าน</a>

		                </div>
	                </div>
	                <div class="col-sm-3">
		                <div class="service wow fadeInUp">
		                    <div class="service-icon"><span><img src="./assets/img/icon/e2.png" width="64px" height="64px"></span></div>
		                    <h3>การออกกำลังกาย</h3>
                        <a class="big-link-1" href="info.php">อ่าน</a>

		                </div>
	                </div>
	                <div class="col-sm-3">
		                <div class="service wow fadeInDown">
		                    <div class="service-icon"><span><img src="./assets/img/icon/e3.png" width="64px" height="64px"></span></div>
		                    <h3>ลดน้ำหนัก</h3>
                        <a class="big-link-1" href="info.php">อ่าน</a>
		                </div>
	                </div>
	            </div>
	        </div>
        </div>



        <!-- Testimonials -->
        <div class="testimonials-container">
	        <div class="container">
	        	<div class="row">
		            <div class="col-sm-12 testimonials-title wow fadeIn">
		                <h2>แนะนำ</h2>
		            </div>
	            </div>
	            <div class="row">
	                <div class="col-sm-10 testimonial-list wow fadeInLeft">
	                	<div role="tabpanel">
	                		<!-- Tab panes -->
	                		<div class="tab-content">
	                			<div role="tabpanel" class="tab-pane fade in active" id="tab1">
	                				<div class="testimonial-image">
	                					<img src="assets/img/chapter/in2.jpg">
	                				</div>
	                				<div class="testimonial-text">
                            <h4>เกี่ยวกับผู้พัฒนา</h4>
		                                <p>Name : Wilailux Hwanngurn - Nikname : Aorair </p>
                                    <p>FB : Air Wilailux</p>
                                    <p>Emil : wilailux.ha.57@ubu.ac.th</p>
                                    <p>เว็บไวค์นี้จัดทำเพื่อการศึกษา รายวิชา Web Programming </p>
                                    <p></p>
	                                </div>
	                			</div>



	                		</div>
	                		<!-- Nav tabs -->

	                	</div>
	                </div>
	                <div class="col-sm-2 testimonial-icon wow fadeInUp">
	                	<div>
	                		<span aria-hidden="true" class="icon_pushpin"></span>
	                	</div>
	                </div>
	            </div>
	        </div>
        </div>

        <!-- Footer -->
        <footer>
            <div class="container">

                <div class="row">
                    <div class="col-sm-7 footer-copyright wow fadeIn">
                        <p></p>
                    </div>

                </div>
            </div>
        </footer>

        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/bootstrap-hover-dropdown.min.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/retina-1.1.0.min.js"></script>
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <script src="assets/flexslider/jquery.flexslider-min.js"></script>
        <script src="assets/js/jflickrfeed.min.js"></script>
        <script src="assets/js/masonry.pkgd.min.js"></script>
        <script src="assets/js/jquery.ui.map.min.js"></script>
        <script src="assets/js/scripts.js"></script>

    </body>

</html>
