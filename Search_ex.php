<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>info</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:700,400">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/elegant-font/code/style.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <link rel="stylesheet" href="assets/css/magnific-popup.css">
        <link rel="stylesheet" href="assets/flexslider/flexslider.css">
        <link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/media-queries.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    <!-- Theme CSS -->
    <link href="assets/css/agency.min.css" rel="stylesheet">

    </head>

    <body>

      <!-- Top menu -->
  <nav class="navbar" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.html"></a>
      </div><br>
      <!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="top-navbar-1">
          <ul class="nav navbar-nav navbar-right">
						<li>
							<a href="index.php"><span aria-hidden="true" class="icon_house"></span><br>หน้าแรก</a>
						</li>
						<li class="active">
							<a href="Cal_food.php"><span aria-hidden="true" class="glyphicon glyphicon-th-list"></span><br>ตารางแคล</a>
						</li>
						<li >
							<a href="info.php"><span aria-hidden="true" class="glyphicon glyphicon-bullhorn"></span><br>สาระสุขภาพ</a>
						</li>
						<li>
							<a href="Cal.php"><span aria-hidden="true" class="glyphicon glyphicon-calendar"></span><br>โปรแกรมคำนวณ</a>
						</li>
						<li>
							<a class="region-inner"  href="login.php"><span aria-hidden="true" class="icon_profile"></span><br>เข้าสู่ระบบ</a>
						</li>
						<li>
							<a href="register.php"><span aria-hidden="true" class="glyphicon glyphicon-edit"></span><br>สมัครสมาชิก</a>
						</li>

					</ul>
				</div>
			</div>
		</nav>

        <!-- Page Title -->
        <div class="page-title-container">
            <div class="container">
                  <div class="row">

                        <div class="col-sm-10 col-sm-offset-1 page-title wow fadeIn">
                            <h2><center>พลังงานที่ใช้ในการทำกิจกรรม</center></h2>


                    </div>
                    <div class="col-sm-3 col-sm-offset-2">
                        <a href="Cal_food.php"><div class="service wow fadeInUp">
                            <div class="service-icon"><span><img src="./assets/img/icon/cf.png" width="64px" height="64px"></span></div>
                            <h3 >ปริมาณแคลอรี่ในอาหาร </h3>
                        </div></a>
                      </div>
                      <div class="col-sm-3 col-sm-offset-2">
                          <a href="Cal_ex.php"><div class="service wow fadeInUp">
                              <div class="service-icon"><span><img src="./assets/img/icon/ce.png" width="64px" height="64px"></span></div>
                              <h3 style="background-color:#fedc8b">พลังงานที่ใช้ในการทำกิจกรรม</h3>
                          </div></a>
                        </div>
                      </div><br>

                  </div>

                </div>

         <!--  -->
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1  wow fadeIn">
                  <form name="frm" method="get" action="Search_ex.php"><br>
                      <div class="input-group">
                        <input type="Search" name="txtkey" placeholder="ชื่อกิจกรรม..." class="form-control" value="<?php echo $_GET["txtkey"];?>">
                        <div class="input-group-addon">
                        <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                        </div>
                      </div>
                    </form>
                    </div>
                  </div>
              </div><br>
           <?php
          if($_GET["txtkey"] != "")
          	{
              include "config.php";
              $objConnect = mysql_connect("$servername","$username","$password") or die("Error Connect to Database");
              $objDB = mysql_select_db("$dbname");
          	$strSQL = "SELECT * FROM cal_ex WHERE (name LIKE '%".$_GET["txtkey"]."%')";
          	$objQuery = mysql_query($strSQL) or die ("Error Query [".$strSQL."]");
          	?>

              <div class="col-sm-10 col-sm-offset-1 ">
                  <div class="panel panel-default col-sm-10 col-sm-offset-1">

                      <!-- /.panel-tabel -->
                      <div class="panel-body">
                          <div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">

                        <div class="row"><div class="col-sm-10 col-sm-offset-1">
                          <!-- Connect db by http://www.thaicreate.com/php/php-mysql-list-record.html-->

                          <table width="100%" class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info" style="width: 100%;">
                              <thead>
                                  <tr role="row">
                                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"  style="width: 340px;" bgcolor="#fedc8b">
                                    กิจกรรม 1 ชั่วโมง</th>
                                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 140px;" bgcolor="#fedc8b">
                                    เผาผลาญ/กิโลแคลอรี่</th>
                                  </tr>
                              </thead>

                        <?php
                        if(mysql_num_rows($objQuery) > 0){
                          while($objResult = mysql_fetch_array($objQuery)) { ?>
                              <tbody>
                              <tr class="gradeA odd" role="row">
                                <td><?php echo $objResult["name"];?></td>
                                <td><?php echo $objResult["cal"];?></td>

                                  </tr>

                                </tbody>
                                <?php } ?>
                          </table>
                          <?php }else {
                            echo "<h3>ไม่พบข้อมูล</h3>"; }?>


                        <?php mysql_close($objConnect); } ?>

                      </div>
                      <!-- /.panel-body -->
                  </div>
                  <!-- /.panel -->
              </div>
              <!-- /.col-lg-12 -->




        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/bootstrap-hover-dropdown.min.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/retina-1.1.0.min.js"></script>
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <script src="assets/flexslider/jquery.flexslider-min.js"></script>
        <script src="assets/js/jflickrfeed.min.js"></script>
        <script src="assets/js/masonry.pkgd.min.js"></script>
        <script src="assets/js/jquery.ui.map.min.js"></script>
        <script src="assets/js/scripts.js"></script>

    </body>

</html>
