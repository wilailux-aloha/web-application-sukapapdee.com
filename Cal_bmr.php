<meta charset="utf-8">
<?php session_start(); ?>
<?php
  if (!$_SESSION["UserID"]){  //check session
    echo "<script>";
    echo "alert(\" ขอสงวนสิทธฺิ์การใช้งานหน้านี้สำหรับสมาชิก! กรุณาเข้าสู่ระบบ หรือสมัครสมาชิกก่อนค่ะ \");";
    echo "window.location=\"login.php\";";
    echo "</script>";
  }else {?>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>info</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:700,400">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/elegant-font/code/style.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <link rel="stylesheet" href="assets/css/magnific-popup.css">
        <link rel="stylesheet" href="assets/flexslider/flexslider.css">
        <link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/media-queries.css">
        <!-- <link rel="stylesheet" href="assets/css/style_from.css"> -->

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    <!-- Theme CSS -->
    <link href="assets/css/agency.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>



    <style>

    #result-panel {
      /*visibility: hidden;*/
      display:none;
    }
    #result {
      font-size: 20px;
      font-family: 'Montserrat', sans-serif;
      text-align:center;
    }
    .flip {
    padding: 20px;
    text-align: center;
    background-color: #ffe5b4;
    border: solid 1px #c3c3c3;
    }

    </style>


    </head>

    <body>

      <!-- Top menu -->
  <nav class="navbar" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.html"></a>
      </div><br>
      <!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="top-navbar-1">
          <ul class="nav navbar-nav navbar-right">
						<li>
							<a href="index.php"><span aria-hidden="true" class="icon_house"></span><br>หน้าแรก</a>
						</li>
						<li>
							<a href="Cal_food.php"><span aria-hidden="true" class="glyphicon glyphicon-th-list"></span><br>ตารางแคล</a>
						</li>
						<li >
							<a href="info.php"><span aria-hidden="true" class="glyphicon glyphicon-bullhorn"></span><br>สาระสุขภาพ</a>
						</li>
						<li class="active">
							<a href="Cal.php"><span aria-hidden="true" class="glyphicon glyphicon-calendar"></span><br>โปรแกรมคำนวณ</a>
						</li>

						<li>
							<a href="register.php"><span aria-hidden="true" class="glyphicon glyphicon-edit"></span><br>สมัครสมาชิก</a>
						</li>

					</ul>
				</div>
			</div>
		</nav>

    <!-- Page Title -->
    <div class="page-title-container">
        <div class="container">
              <div class="row">
                    <div class="col-sm-10 col-sm-offset-1  wow fadeIn">
                        <h3><center>ยินดีต้อนรับคุณ <?= $_SESSION['user_name']?> ^ ^</center></h3>
                        <!-- <?php print_r($_SESSION);?> -->

                </div>

                  </div>
              </div>
            </div><!--  -->

            <div class="container">
                  <div class="row">
                        <div class="col-sm-9 col-sm-offset-7">
                          <a href="EditRecord.php"><img src ="./assets/img/icon/edit.png">แก้ไขข้อมูล</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          <a href="logout.php" onclick="out()"><img src ="./assets/img/icon/u3.png">ออกจากระบบ</a>

                    </div>

                      </div>
                  </div>
                  <script type="text/javascript">
                  function out(){
                    alert("แน่ใจว่าคุณต้องการออกจากระบบ?")
                  }
                  </script>


                <div class="col-sm-10 col-sm-offset-1  wow fadeIn">
                  <div class="container">
                      <div class="row">
                  <div class="col-md-3">
                      <div class="panel panel-default panel-box">
                          <a href="#" class="btn btn-lg btn-danger btn-block">โปรแกรมคำนวนร่างกาย</a>
                          <div style="height: 25px;"></div>
                          <div class="btn-group-vertical">
                            <ul class="mail-ul nav">
                              <li><a href="Cal.php" class="list-group-item list-group-item-action list-group-item-warning">ดัชนีมวลกาย(BMI)</a></li>
                              <div style="height: 10px;"></div>
                               <li><a href="Cal_bmr.php" class="list-group-item list-group-item-action list-group-item">อัตราการเผาผลาญพลังงาน(BMR)</a></li>
                               <div style="height: 10px;"></div>
                                <li><a href="Cal_day.php" class="list-group-item list-group-item-action list-group-item-warning ">แคลอรี่ที่เหมาะสมต่อวัน</a></li>
                                <div style="height: 10px;"></div>
                                 <li><a href="Cal_weight.php" class="list-group-item list-group-item-action list-group-item-warning">คำนวณน้ำหนักที่เหมาะสม</a></li>
                                 <div style="height: 25px;"></div>
                          </ul>
                          </div>
                      </div>
                  </div>
                  <div class="panel col-md-9">
                    <div class="panel-box">
                    <center><h4>BMR Calculator</h4><a>** คำนวนอัตราการเผาผลาญพลังงานของคุณ **</a></center><br><br>
                          <div class="col-sm-5 " >
                            <form id="myForm">
                            <label for = "leeftijd" class="col-sm-4 control-label">อายุ(ปี)</label>
                                        <input type = "number" class = "form-control" id = "leeftijd">
                                        <p></p>
                                        <label for="gewicht2"class="col-sm-5 control-label">น้ำหนัก(kg.)</label>
                                        <input type = "number" class = "form-control" id = "gewicht2">
                                        <p></p>
                                        <label for = "lengte2" class="col-sm-5 control-label">ส่วนสูง(cm.)</label>
                                        <input type = "number" class = "form-control" id = "lengte2">
                                        <p></p><br>
                                        <label for = "man"></label>เพศ : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type = "radio" name ="geslacht" id = "man" value = "man"/>ชาย
                                        <label for = "Lady"></label>&nbsp;&nbsp;
                                        <input type = "radio" name ="geslacht" id = "Lady" value = "Lady"/>หญิง
                                        <p></p><br>

                                        <button class = "btn btn-warning" type = "button" id = "button2">ยืนยัน</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <button class = "btn btn-warning" type = "button" onclick="myFunction()">รีเซ็ต</button><br><br>
                                    </form>
                              </div>

                              <div class="col-sm-6 col-sm-offset-1 " id="result-panel">
                                <p>ค่า BMR แคลอรี่/วัน ของคุณ </p>
                                <p class="flip" id="result"></p><br>
                                <p>BMR(Basal Metabolic Rate)หรือ อัตราการเผาผลาญพลังงานของร่างกาย
                                  “ขณะพักผ่อน” คือเมื่อร่างกายอยู่นิ่งๆ ไม่มีกิจกรรมอื่นๆ เป็นขณะที่ระบบย่อยพักงาน ซึ่งเป็นพลังงานที่ถูกใช้ไป
                                  เพียงเพื่อรักษาระดับการมีชีวิต เช่น หัวใจเต้น ปอด ตับ กล้ามเนื้อ ผิวหนัง และ อื่นๆ</p>
                            </div>
                    </div>
                      </div>
                    </div>
                  </div>
          </div>


        <!-- Javascript -->
        <!-- <script src="../js/jquery-1.11.1.min.js"></script> -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/bootstrap-hover-dropdown.min.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/retina-1.1.0.min.js"></script>
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <script src="assets/flexslider/jquery.flexslider-min.js"></script>
        <script src="assets/js/jflickrfeed.min.js"></script>
        <script src="assets/js/masonry.pkgd.min.js"></script>
        <script src="assets/js/jquery.ui.map.min.js"></script>
        <script src="assets/js/scripts.js"></script>

        <!-- <script src="jquery-1.11.1.min.js"></script> -->
        <script>
        $(document).ready(
          $("#button2").on("click", chek)

        );

        function myFunction() {
            document.getElementById("myForm").reset();
        }

        function chek() {
          var length = $("#lengte2").val();
          var weight = $("#gewicht2").val();
          var age = $("#leeftijd").val();
          var gender = $("input[name='geslacht']:checked").val();


          if (length<=100 || length>=250) {
              alert ("กรุณาใส่ส่วนสูงให้มีค่าระหว่าง 100 – 250 cm !");
          }else if(gender !="man" && gender !="Lady"){
              alert ("กรุณาเลือกเพศ");
          }else if(weight<=30 || weight>=200){
              alert ("กรุณาใส่น้ําหนักให้มีค่าระหว่าง 30 – 200 kg!");
          }else if(age<=10 || age>=120){
              alert ("กรุณาใส่อายุให้มีค่าระหว่าง 10 – 120 ปี");
          }else {
            berekenBMR();
          }
        }

        function berekenBMR(){
        var length = $("#lengte2").val();
        var weight = $("#gewicht2").val();
        var age = $("#leeftijd").val();
        var gender = $("input[name='geslacht']:checked").val();
        var BMRCalculation = 0;
        $("#result-panel").fadeIn(1000);

        if (gender == "man"){
          BMRCalculation = 66 + (13.7 * weight) + (5*length) - (6.8*age);
        }
        else{
          BMRCalculation = 665 + (9.6 * weight) + (1.8*length) - (4.7*age);
        }
        document.getElementById('result').innerHTML = BMRCalculation.toFixed(2);
        }
        </script>

    </body>

</html>
<?php }?>
