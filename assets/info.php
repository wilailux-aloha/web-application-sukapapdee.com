<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>info</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:700,400">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/elegant-font/code/style.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <link rel="stylesheet" href="assets/css/magnific-popup.css">
        <link rel="stylesheet" href="assets/flexslider/flexslider.css">
        <link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/media-queries.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    <!-- Theme CSS -->
    <link href="assets/css/agency.min.css" rel="stylesheet">

    </head>

    <body>

      <!-- Top menu -->
  <nav class="navbar" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.html"></a>
      </div><br>
      <!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="top-navbar-1">
          <ul class="nav navbar-nav navbar-right">
						<li>
							<a href="index.html"><span aria-hidden="true" class="icon_house"></span><br>หน้าแรก</a>
						</li>
						<li>
							<a href="Cal_food.php"><span aria-hidden="true" class="glyphicon glyphicon-th-list"></span><br>ตารางแคล</a>
						</li>
						<li class="active">
							<a href="info.html"><span aria-hidden="true" class="glyphicon glyphicon-bullhorn"></span><br>ความรู้</a>
						</li>
						<li>
							<a href="Cal.php"><span aria-hidden="true" class="glyphicon glyphicon-calendar"></span><br>โปรแกรมคำนวณ</a>
						</li>
						<li>
							<a class="region-inner"  href="login.php"><span aria-hidden="true" class="icon_profile"></span><br>เข้าสู่ระบบ</a>
						</li>
						<li>
							<a href="register.php"><span aria-hidden="true" class="glyphicon glyphicon-edit"></span><br>สมัครสมาชิก</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>

        <!-- Page Title -->
        <div class="page-title-container">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 page-title wow fadeIn">
                        <h2><center>ความรู้ทั่วไปเกี่ยวกับสุขภาพ</center></h2>

                    </div>
                </div>
            </div>
        </div>

        <!-- Portfolio -->
        <div class="portfolio-container">
	        <div class="container">

                <!-- Services -->
                <div class="services-container">

        	            <div class="row">
        	            	<div class="col-sm-3">
        		                <div class="service wow fadeInUp">
        		                    <div class="service-icon"><span><img src="./assets/img/icon/k.png" width="64px" height="64px"></span></div>
        		                    <h3>ความรู้ทั่วไป</h3>
        		                    <a class="big-link-1" href="#info">อ่าน</a>
        		                </div>
        					</div>
        					<div class="col-sm-3">
        		                <div class="service wow fadeInDown">
        		                    <div class="service-icon"><span><img src="./assets/img/icon/f.png" width="70px" height="70px"></span></div>
        		                    <h3>อาหารการกิน</h3>
                                <a class="big-link-1" href="#food">อ่าน</a>

        		                </div>
        	                </div>
        	                <div class="col-sm-3">
        		                <div class="service wow fadeInUp">
        		                    <div class="service-icon"><span><img src="./assets/img/icon/e2.png" width="64px" height="64px"></span></div>
        		                    <h3>การออกกำลังกาย</h3>
                                <a class="big-link-1" href="#exercise">อ่าน</a>

        		                </div>
        	                </div>
        	                <div class="col-sm-3">
        		                <div class="service wow fadeInDown">
        		                    <div class="service-icon"><span><img src="./assets/img/icon/e3.png" width="64px" height="64px"></span></div>
        		                    <h3>ลดน้ำหนัก</h3>
                                <a class="big-link-1"  href="#Weight-loss">อ่าน</a>
        		                </div>
        	                </div>
        	            </div>
                      <!-- Connect DB -->
                      <?php
                      include "config.php";
                      $objConnect = mysql_connect("$servername","$username","$password") or die("Error Connect to Database");
                      $objDB = mysql_select_db("$dbname");
                      $strSQL = "SELECT * FROM info ";
                      $objQuery = mysql_query($strSQL) or die ("Error Query [".$strSQL."]")
                      ?>
                      <!--  -->


                      <!-- section info -->
                      <section id ="info">
                      <div class="row">
        	            	<div class="col-sm-3">

                            <div class="work wow fadeInUp">
        		                    <img src="./assets/img/chapter/info1.jpg">
        		                    <h3>8 วิธียกเครื่องระบบเผาผลาญใหม่ให้ได้ผลยั่งยืน</h3>
        		                    <p>เราออกกำลังกายก็เพื่อทำให้ระบบการเผาผลาญของร่างกายดีขึ้น ซึ่งข้อดีของมัน
                                  <a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">อ่านต่อ . . .</a></p>

        		                </div>
        	                </div>
        	                <div class="col-sm-3">
        		                <div class="work wow fadeInDown">
        		                    <img src="./assets/img/chapter/ch5.jpg">
        		                    <h3>7 อาหารเย็นแคลรอลี่ต่ำ</h3>
        		                    <p>เซ็ตอาหารเย็นให้สาวๆได้เลือกทานกันตามถนัดทั้ง 7วันเลยล่ะค่ะ!งานนี้บอกเลย <a href="#">อ่านต่อ . . .</a></p>
        		                    <div class="work-bottom">
        		                        <a class="big-link-2 view-work" href="assets/img/portfolio/work2.jpg"><span class="icon_search"></span></a>
        		                        <a class="big-link-2" href="portfolio.html"><span class="icon_link"></span></a>

        		                    </div>
        		                </div>
        		            </div>
        		            <div class="col-sm-3">
        		                <div class="work wow fadeInUp">
        		                    <img src="./assets/img/chapter/ch3.jpg">
        		                    <h3>อยากหุ่นสวยต้องทานโปรตีน</h3>
        		                    <p>อยากมีหุ่นสวยเฟิร์ม นอกจากการออกกำลังกายและการพักผ่อนที่เพียงพอแล้ว การรับประทานโปรตีนในปริมาณพอเหมาะสามารถช่วยสร้างกล้ามเนื้อให้แข็ง
                                  <a href="#">อ่านต่อ . . .</a></p>
        		                    <div class="work-bottom">
        		                        <a class="big-link-2 view-work" href="assets/img/portfolio/work3.jpg"><span class="icon_search"></span></a>
        		                        <a class="big-link-2" href="portfolio.html"><span class="icon_link"></span></a>
        		                    </div>
        		                </div>
        		            </div>
        		            <div class="col-sm-3">
        		                <div class="work wow fadeInDown">
        		                    <img src="./assets/img/chapter/ch4.jpg">
        		                    <h3>ลดน้ำหนักอย่าใจร้อน</h3>
        		                    <p>สำหรับหลายคนเมื่อต้องการลดน้ำหนักมักจะเริ่งรีบใจร้อน อยากให้ลดได้ 5 กิโลกรัมภาย <a href="#">อ่านต่อ . . .</a></p>
        		                    <div class="work-bottom">
        		                        <a class="big-link-2 view-work" href="assets/img/portfolio/work4.jpg"><span class="icon_search"></span></a>
        		                        <a class="big-link-2" href="portfolio.html"><span class="icon_link"></span></a>
        		                    </div>
        		                </div>
        		            </div>
        	            </div>
                    <a href=""><img src="./assets/img/icon/up.png"></a>
                  </section>

                      <!-- section food -->
                      <section id ="food">
                      <div class="row">
        	            	<div class="col-sm-3">
                            <div class="work wow fadeInUp">
        		                    <img src="./assets/img/chapter/ch2.png">
        		                    <h3>อาหารเย็นไม่เกิน 200 kcal</h3>
        		                    <p> ถ้าอยากผอม นอกจาก การออกกำลังกาย แล้ว การนับแคลอรีในการกินแต่ละวัน <a href="#">อ่านต่อ . . .</a></p>
        		                    <div class="work-bottom">
        		                        <a class="big-link-2 view-work" href="assets/img/portfolio/work1.jpg"><span class="icon_search"></span></a>
        		                        <a class="big-link-2" href="portfolio.html"><span class="icon_link"></span></a>
        		                    </div>
        		                </div>
        	                </div>
        	                <div class="col-sm-3">
        		                <div class="work wow fadeInDown">
        		                    <img src="./assets/img/chapter/ch5.jpg">
        		                    <h3>7 อาหารเย็นแคลรอลี่ต่ำ</h3>
        		                    <p>เซ็ตอาหารเย็นให้สาวๆได้เลือกทานกันตามถนัดทั้ง 7วันเลยล่ะค่ะ!งานนี้บอกเลย <a href="#">อ่านต่อ . . .</a></p>
        		                    <div class="work-bottom">
        		                        <a class="big-link-2 view-work" href="assets/img/portfolio/work2.jpg"><span class="icon_search"></span></a>
        		                        <a class="big-link-2" href="portfolio.html"><span class="icon_link"></span></a>

        		                    </div>
        		                </div>
        		            </div>
        		            <div class="col-sm-3">
        		                <div class="work wow fadeInUp">
        		                    <img src="./assets/img/chapter/ch3.jpg">
        		                    <h3>อยากหุ่นสวยต้องทานโปรตีน</h3>
        		                    <p>อยากมีหุ่นสวยเฟิร์ม นอกจากการออกกำลังกายและการพักผ่อนที่เพียงพอแล้ว การรับประทานโปรตีนในปริมาณพอเหมาะสามารถช่วยสร้างกล้ามเนื้อให้แข็ง
                                  <a href="#">อ่านต่อ . . .</a></p>
        		                    <div class="work-bottom">
        		                        <a class="big-link-2 view-work" href="assets/img/portfolio/work3.jpg"><span class="icon_search"></span></a>
        		                        <a class="big-link-2" href="portfolio.html"><span class="icon_link"></span></a>
        		                    </div>
        		                </div>
        		            </div>
        		            <div class="col-sm-3">
        		                <div class="work wow fadeInDown">
        		                    <img src="./assets/img/chapter/ch4.jpg">
        		                    <h3>ลดน้ำหนักอย่าใจร้อน</h3>
        		                    <p>สำหรับหลายคนเมื่อต้องการลดน้ำหนักมักจะเริ่งรีบใจร้อน อยากให้ลดได้ 5 กิโลกรัมภาย <a href="#">อ่านต่อ . . .</a></p>
        		                    <div class="work-bottom">
        		                        <a class="big-link-2 view-work" href="assets/img/portfolio/work4.jpg"><span class="icon_search"></span></a>
        		                        <a class="big-link-2" href="portfolio.html"><span class="icon_link"></span></a>
        		                    </div>
        		                </div>
        		            </div>
        	            </div>
                    <a href=""><img src="./assets/img/icon/up.png"></a>
                  </section>


                      <!--  -->
                      <section id="exercise">
                      <div class="row">
        	            	<div class="col-sm-3">

                            <div class="work wow fadeInUp">
        		                    <img src="./assets/img/chapter/ch2.png">
        		                    <h3>อาหารเย็นไม่เกิน 200 kcal</h3>
        		                    <p> ถ้าอยากผอม นอกจาก การออกกำลังกาย แล้ว การนับแคลอรีในการกินแต่ละวัน <a href="#">อ่านต่อ . . .</a></p>
        		                    <div class="work-bottom">
        		                        <a class="big-link-2 view-work" href="assets/img/portfolio/work1.jpg"><span class="icon_search"></span></a>
        		                        <a class="big-link-2" href="portfolio.html"><span class="icon_link"></span></a>
        		                    </div>
        		                </div>
        	                </div>
        	                <div class="col-sm-3">
        		                <div class="work wow fadeInDown">
        		                    <img src="./assets/img/chapter/ch5.jpg">
        		                    <h3>7 อาหารเย็นแคลรอลี่ต่ำ</h3>
        		                    <p>เซ็ตอาหารเย็นให้สาวๆได้เลือกทานกันตามถนัดทั้ง 7วันเลยล่ะค่ะ!งานนี้บอกเลย <a href="#">อ่านต่อ . . .</a></p>
        		                    <div class="work-bottom">
        		                        <a class="big-link-2 view-work" href="assets/img/portfolio/work2.jpg"><span class="icon_search"></span></a>
        		                        <a class="big-link-2" href="portfolio.html"><span class="icon_link"></span></a>

        		                    </div>
        		                </div>
        		            </div>
        		            <div class="col-sm-3">
        		                <div class="work wow fadeInUp">
        		                    <img src="./assets/img/chapter/ch3.jpg">
        		                    <h3>อยากหุ่นสวยต้องทานโปรตีน</h3>
        		                    <p>อยากมีหุ่นสวยเฟิร์ม นอกจากการออกกำลังกายและการพักผ่อนที่เพียงพอแล้ว การรับประทานโปรตีนในปริมาณพอเหมาะสามารถช่วยสร้างกล้ามเนื้อให้แข็ง
                                  <a href="#">อ่านต่อ . . .</a></p>
        		                    <div class="work-bottom">
        		                        <a class="big-link-2 view-work" href="assets/img/portfolio/work3.jpg"><span class="icon_search"></span></a>
        		                        <a class="big-link-2" href="portfolio.html"><span class="icon_link"></span></a>
        		                    </div>
        		                </div>
        		            </div>
        		            <div class="col-sm-3">
        		                <div class="work wow fadeInDown">
        		                    <img src="./assets/img/chapter/ch4.jpg">
        		                    <h3>ลดน้ำหนักอย่าใจร้อน</h3>
        		                    <p>สำหรับหลายคนเมื่อต้องการลดน้ำหนักมักจะเริ่งรีบใจร้อน อยากให้ลดได้ 5 กิโลกรัมภาย <a href="#">อ่านต่อ . . .</a></p>
        		                    <div class="work-bottom">
        		                        <a class="big-link-2 view-work" href="assets/img/portfolio/work4.jpg"><span class="icon_search"></span></a>
        		                        <a class="big-link-2" href="portfolio.html"><span class="icon_link"></span></a>
        		                    </div>
        		                </div>
        		            </div>
        	            </div>
                      <a href=""><img src="./assets/img/icon/up.png"></a>
                  </section>

                      <!-- section Weight-loss -->
                      <section id ="Weight-loss">
                      <div class="row">
        	            	<div class="col-sm-3">

                            <div class="work wow fadeInUp">
        		                    <img src="./assets/img/chapter/ch2.png">
        		                    <h3>อาหารเย็นไม่เกิน 200 kcal</h3>
        		                    <p> ถ้าอยากผอม นอกจาก การออกกำลังกาย แล้ว การนับแคลอรีในการกินแต่ละวัน <a href="#">อ่านต่อ . . .</a></p>
        		                    <div class="work-bottom">
        		                        <a class="big-link-2 view-work" href="assets/img/portfolio/work1.jpg"><span class="icon_search"></span></a>
        		                        <a class="big-link-2" href="portfolio.html"><span class="icon_link"></span></a>
        		                    </div>
        		                </div>
        	                </div>
        	                <div class="col-sm-3">
        		                <div class="work wow fadeInDown">
        		                    <img src="./assets/img/chapter/ch5.jpg">
        		                    <h3>7 อาหารเย็นแคลรอลี่ต่ำ</h3>
        		                    <p>เซ็ตอาหารเย็นให้สาวๆได้เลือกทานกันตามถนัดทั้ง 7วันเลยล่ะค่ะ!งานนี้บอกเลย <a href="#">อ่านต่อ . . .</a></p>
        		                    <div class="work-bottom">
        		                        <a class="big-link-2 view-work" href="assets/img/portfolio/work2.jpg"><span class="icon_search"></span></a>
        		                        <a class="big-link-2" href="portfolio.html"><span class="icon_link"></span></a>

        		                    </div>
        		                </div>
        		            </div>
        		            <div class="col-sm-3">
        		                <div class="work wow fadeInUp">
        		                    <img src="./assets/img/chapter/ch3.jpg">
        		                    <h3>อยากหุ่นสวยต้องทานโปรตีน</h3>
        		                    <p>อยากมีหุ่นสวยเฟิร์ม นอกจากการออกกำลังกายและการพักผ่อนที่เพียงพอแล้ว การรับประทานโปรตีนในปริมาณพอเหมาะสามารถช่วยสร้างกล้ามเนื้อให้แข็ง
                                  <a href="#">อ่านต่อ . . .</a></p>
        		                    <div class="work-bottom">
        		                        <a class="big-link-2 view-work" href="assets/img/portfolio/work3.jpg"><span class="icon_search"></span></a>
        		                        <a class="big-link-2" href="portfolio.html"><span class="icon_link"></span></a>
        		                    </div>
        		                </div>
        		            </div>
        		            <div class="col-sm-3">
        		                <div class="work wow fadeInDown">
        		                    <img src="./assets/img/chapter/ch4.jpg">
        		                    <h3>ลดน้ำหนักอย่าใจร้อน</h3>
        		                    <p>สำหรับหลายคนเมื่อต้องการลดน้ำหนักมักจะเริ่งรีบใจร้อน อยากให้ลดได้ 5 กิโลกรัมภาย <a href="#">อ่านต่อ . . .</a></p>
        		                    <div class="work-bottom">
        		                        <a class="big-link-2 view-work" href="assets/img/portfolio/work4.jpg"><span class="icon_search"></span></a>
        		                        <a class="big-link-2" href="portfolio.html"><span class="icon_link"></span></a>
        		                    </div>
        		                </div>
        		            </div>
        	            </div>
                      <a href=""><img src="./assets/img/icon/up.png"></a>
                  </section>
                </div>
	        </div>
        </div>

        <!-- Footer -->
        <footer>

                <div class="row">
                    <div class="col-sm-7 footer-copyright wow fadeIn">
                        <p>&copy; Gioia - All rights reserved. Template by <a href="http://azmind.com/free-bootstrap-themes-templates/">Azmind</a>.</p>
                    </div>

                </div>
            </div>
        </footer>

        <!-- info1 -->
    <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-10 col-sm-offset-1">
                            <div class="modal-body">
                                <!-- 8 วิธียกเครื่องระบบเผาผลาญใหม่ให้ได้ผลยั่งยืน -->
                                <h3>8 วิธียกเครื่องระบบเผาผลาญใหม่ให้ได้ผลยั่งยืน</h3>
                                <img src="./assets/img/chapter/info1.jpg">
                                <p>เราออกกำลังกายก็เพื่อทำให้ระบบการเผาผลาญของร่างกายดีขึ้น ซึ่งข้อดีของมันคือการเผาผลาญไขมันส่วนเกินก็จะทำงานได้อย่างมีประสิทธิภาพมากขึ้นนั่นเอง
                                  คำแนะนำวิธีเพิ่มเมตาโบลิซึมให้กับร่างกายอย่างง่ายๆ ตามมาดูกันค่ะว่ามีอะไรบ้าง
                                </p><br>
                                <p><strong>1. เล่นเวทสร้างกล้ามเนื้อ</strong></p>
                                <p>
                                  “กล้ามเนื้อเยอะขึ้น = เผาผลาญเยอะขึ้น” แม้จะทราบดีว่าการออกกำลังกายแบบคาร์ดิโอ จะช่วยทำให้อัตราการเผาผลาญของร่างกายดีขึ้นก็จริง แต่การมีกล้ามเนื้อที่แข็งแรง ก็เหมือนพื้นฐานของการเล่นได้ยาวนาน ฉนั้นก่อนเราจะพัฒนาการเล่นแบบ คาร์ดิโอ เราต้องมั่นใจว่ากล้ามเนื้อเราแข็งแรง ด้วยการเริ่มเล่นเวทไปด้วย เพราะกล้ามเนื้อที่สร้างขึ้นคือเนื้อเยื่อที่ถูกกระตุ้น และการเล่นเวทสร้างกล้ามเนื้อจะช่วยทำให้ระบบการเผาผลาญพลังงานในร่างกายของเราดีขึ้นด้วยค่ะ แต่เคล็ดลับก็คือ ทุกครั้งที่มีการบริหารสร้างกล้ามเนื้อด้วยการเล่นเวท อย่าลืมจัดสรรการรับประทานอาหารให้สมดุลย์ เพื่อให้ร่างกายมีพลังงานเพียงพอต่อการใช้งานในแต่ละวัน
                                </p><br>
                                <p><strong>2. อินเทอวัล เทรนนิ่ง</strong></p>
                                <p>
                                  นอกจากนั้นการเล่นเวท ยังเป็นกิจกรรมที่ท้าทายให้เราไต่ระดับน้ำหนักที่เพิ่มมากขึ้นเรื่อยๆ และถ้าหากเล่นเวทจนอยู่ตัวแล้ว แนะนำให้เริ่มต้นออกกำลังกายแบบ แบบ อินเทอวัล เทรนนิ่ง หรือคาร์ดิโอ ที่เรารู้จักกัน ครั้งละ 20-25 นาที หรือ ลอง T25 ไม่ว่าจะเป็นการวิ่งสายพาน ใช้เครื่องไต่เขา หรือกระโดดเชือก โดยการออกกำลังกายลักษณะนี้จะใช้เวลาไม่นาน แต่เพิ่มความเร็ว และความหนักมากกว่าการออกกำลังกายแบบปกติเป็นเท่าตัว ซึ่งข้อดีของมันนอกจากจะเป็นการบริหารกล้ามเนื้อหัวใจแล้ว ยังช่วยพัฒนากล้ามเนื้อ ทำให้รูปรางดีขึ้นอย่างเห็นได้ชัด และอย่าลืมค่ะว่า หลังจากจบเซ็ต อินเทอวัล เทรนนิ่งแล้ว ต้องใช้เวลา Cool down เพื่อปรับสภาพร่างกายก่อนจะทำกิจกรรมอื่นๆ ต่อไปด้วยค่ะ
                                </p><br>
                                <p><strong>3. ทานโปรตีนรักษาสมดุล</strong></p>
                                <p>
                                  การเพิ่มกระบวนการเมตาบอลิซึมให้ร่างกายด้วยการออกกำลังกาย และสร้างกล้ามเนื้อจำเป็นต้องทำไปพร้อมกับการตรวจสอบปริมาณโปรตีนที่เรารับประทานเข้าไปด้วยค่ะ เพราะกล้ามเนื้อที่มาแทนที่ไขมัน จะต้องมาจากโปรตีนที่พอดี หากร่างกายได้โปรตีนไม่เพียงพอต่อการออกกำลังกย ก็จะส่งผลเป็นลูกโซ่ ให้สร้างกล้ามเนื้อได้ช้า เรี่ยวแรงน้อย และ เผาผลาญได้น้อยลงไปอีก ดังนั้น อย่าลืมเติมปัจจัยหลักกับสิ่งที่จะสร้างประสิทธิเตาเผาร่างกายให้พอเพียงด้วยนะคะ คิดง่ายๆ ให้เราทานโปรตีนไม่น้อยกว่า 10-15% ของน้ำหนักตัวในแต่ละวัน เช่นหนัก 50 โล ก็ควรจะได้โปรตีนไม่น้อยกว่า 50 – 75 กรัม ในแต่ละวัน
                                </p><br>
                                <p><strong>4. กาแฟดำ และ ชาเขียว เครื่องดื่มช่วยผลาญพลังงาน</strong></p>
                                <p>
                                  หากคุณเริ่มต้นเช้าวันใหม่ ด้วยชาเขียว หรือกาแฟดำสักแก้ว เราขอบอกว่า คุณมาถูกทางแล้วค่ะ เพราะคาเฟอีนในชา และกาแฟ ไม่ได้เพียงแต่จะทำให้คุณหายจากอาการง่วงเหงาหาวนอนในเวลาเช้าเท่านั้น แต่มันยังช่วยกระตุ้นการเผาผลาญสารอาหารให้กับวันใหม่ของคุณได้เป็นอย่างดีด้วย นอกจากนั้น สารคาเทชิน ที่มีอยู่ในชาเขียว ยังเป็นเคมีพิเศษที่นักโภชนาการทดสอบแล้ว พบว่า ในคนที่ออกกำลังกายสัปดาห์ละ 3 วันและรับประทานชาเขียว อัตราการเผาผลาญพลังงานของพวกเขาจะไปได้ไกลกว่ากลุ่มทดสอบที่ไม่ดื่มอีกด้วย และกาแฟดำ จะมีสาน L-Carnetine ที่ช่วยกระตุ้นร่างกาย ให้อึดระหว่างออกกำลังกาย และ เผาผลาญพลังงานได้ดีอย่างต่อเนื่อง
                                </p><br>
                                <p><strong>5. น้ำเปล่าธรรมดา คือยาวิเศษ</strong></p>
                                <p>วันหนึ่งเราควรดื่มน้ำอย่างน้อยวันละ 8 แก้ว หรือจะเก็บขวดน้ำเอาไว้ข้างตัวเลยได้จะยิ่งดีมากๆ เพราะน้ำจะทำให้ระบบในร่างกายทำงานได้คล่อง และเผาผลาญต่อเนื่องตลอดวัน วิธีที่ดีที่สุดคือ ดื่มทีละนิด และ บ่อยๆตลอดวัน เช่นจิบครึ่งแก้ว ทุกๆครึ่งชั่วโมง
                                </p><br>
                                <p><strong>6. บอกลาสูตรอดอาหาร</strong></p>
                                <p>ไม่ว่าจะเป็นสูตร 3 วัน 5 วัน อดข้าวเย็น งดของทอด อะไรก็ตาม ถ้าหากคุณก้าวเข้ามาในระบบการดูแลอาหาร และเพิ่มกระบวนการเผาผลาญแล้ว ตำราไดเอททั้งหมดคงจะต้องพับไปก่อน เพราะการออกกำลังกาย และการจัดระบบการใช้ชีวิตทั้งหมดข้างต้น จำเป็นต้องใช้ “พลังงาน” ที่เพียงพอ ดังนั้นแทนที่จะ “ลด” หรือ “อด” แนะนำให้คุณจัดตารางการรับประทานอาหารที่มีประโยชน์ และทานให้ครบ 3 มื้อ เพื่อรักษาสมดุลย์ของร่างกาย ให้ร่างกายเคยชินกับการ รับเข้า และเผาผลาญจนเป็นนิสัย
                                </p><br>
                                <p><strong>7. เครียดให้น้อย</strong></p>
                                <p>ลด “ความเครียด” เพราะร่างกายของเราผลิตฮอร์โมน คอร์ติซอล ที่จะมีผลต่อร่างกายในการกักเก็บไขมัน หากเกิดความเครียดสะสมเข้ามากๆ การสะสมไขมันก็จะมากขึ้นตามไปด้วย ดังนั้น การรักษาปริมาณความเครียดแต่พอเหมาะพอดี นอกจากสภาพจิตใจของเราจะดีแล้ว ในระยะยาว ร่างกายของเราก็จะดีไปตามด้วย ลองมองหากิจกรรมคลายเครียด อย่างเช่น การนั่งสมาธิ ทำงานฝีมือ อ่านหนังสือ หรือแม้กระทั่งนั่งจิบชาเบาๆ ในตอนบ่ายวันหยุด แค่นี้ความสุข ก็จะมาพร้อมกับความเพรียว ได้ไม่ยากเลยล่ะค่ะ
                                </p><br>
                                <p><strong>8. นอนให้พอ</strong></p>
                                <p>การนอนคือการฟื้นฟูร่างกาย และจิตใจที่ดีที่สุด ร่างกายเรามีกลไกลมหัศจรรย์ที่สามารถรักษาตัวเองได้เป็นอย่างดี ดังนั้นอย่าละเลยการนอนเด็ดขาด ควรนอนไม่ต่ำกว่าคืนละ 8-10 ชั่วโมง หากคุณตื่นมาแล้วลองสังเกตดูว่า เรารู้สึกสดชื่นพอไหม หรือ ยังเหนื่อยสะสมอ่อนล้า ก็อาจจะต้องชดเชยให้เหมาะสม เพราะการได้พักผ่อนเพียงพอ หมายถึงสภาพร่างกายที่เต็มที่ พร้อมที่จะ Workout และ ทำกิจกรรมได้มากขึ้นตลอดวัน และนั่นคือการกระตุ้น metabolism หรือ ระบบการเผาผลาญของเราอย่างเป็นธรรมชาติ
                                </p><br>


                                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i>ปิด</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/bootstrap-hover-dropdown.min.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/retina-1.1.0.min.js"></script>
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <script src="assets/flexslider/jquery.flexslider-min.js"></script>
        <script src="assets/js/jflickrfeed.min.js"></script>
        <script src="assets/js/masonry.pkgd.min.js"></script>
        <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script src="assets/js/jquery.ui.map.min.js"></script>
        <script src="assets/js/scripts.js"></script>

    </body>

</html>
