<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>info</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:700,400">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/elegant-font/code/style.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <link rel="stylesheet" href="assets/css/magnific-popup.css">
        <link rel="stylesheet" href="assets/flexslider/flexslider.css">
        <link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/media-queries.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    <!-- Theme CSS -->
    <link href="assets/css/agency.min.css" rel="stylesheet">

    </head>

    <body>

      <!-- Top menu -->
  <nav class="navbar" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.html"></a>
      </div><br>
      <!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="top-navbar-1">
          <ul class="nav navbar-nav navbar-right">
						<li>
							<a href="index.php"><span aria-hidden="true" class="icon_house"></span><br>หน้าแรก</a>
						</li>
						<li>
							<a href="Cal_food.php"><span aria-hidden="true" class="glyphicon glyphicon-th-list"></span><br>ตารางแคล</a>
						</li>
						<li class="active">
							<a href="info.php"><span aria-hidden="true" class="glyphicon glyphicon-bullhorn"></span><br>สาระสุขภาพ</a>
						</li>
						<li>
							<a href="Cal.php"><span aria-hidden="true" class="glyphicon glyphicon-calendar"></span><br>โปรแกรมคำนวณ</a>
						</li>
						<li>
							<a class="region-inner"  href="login.php"><span aria-hidden="true" class="icon_profile"></span><br>เข้าสู่ระบบ</a>
						</li>
						<li>
							<a href="register.php"><span aria-hidden="true" class="glyphicon glyphicon-edit"></span><br>สมัครสมาชิก</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>

        <!-- Page Title -->
        <div class="page-title-container">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 page-title wow fadeIn">
                        <h2><center>ความรู้ทั่วไปเกี่ยวกับสุขภาพ</center></h2>

                    </div>
                </div>
            </div>
        </div>

        <!-- Portfolio -->
        <div class="portfolio-container">
	        <div class="container">

                <!-- Services -->
                <div class="services-container">

        	            <div class="row">
                        <div class="col-sm-3">
                          <div class="service wow fadeInUp">
                              <div class="service-icon"><span><img src="./assets/img/icon/e2.png" width="64px" height="64px"></span></div>
                              <h3>การออกกำลังกาย</h3>
                              <a class="big-link-1" href="#exercise">อ่าน</a>

                          </div>
                        </div>

        					        <div class="col-sm-3">
        		                <div class="service wow fadeInDown">
        		                    <div class="service-icon"><span><img src="./assets/img/icon/f.png" width="70px" height="70px"></span></div>
        		                    <h3>อาหารการกิน</h3>
                                <a class="big-link-1" href="#food">อ่าน</a>

        		                </div>
        	                </div>

                          <div class="col-sm-3">
          		                <div class="service wow fadeInUp">
          		                    <div class="service-icon"><span><img src="./assets/img/icon/k.png" width="64px" height="64px"></span></div>
          		                    <h3>ความรู้ทั่วไป</h3>
          		                    <a class="big-link-1" href="#general">อ่าน</a>
          		                </div>
          					       </div>

        	                <div class="col-sm-3">
        		                <div class="service wow fadeInDown">
        		                    <div class="service-icon"><span><img src="./assets/img/icon/e3.png" width="64px" height="64px"></span></div>
        		                    <h3>ลดน้ำหนัก</h3>
                                <a class="big-link-1"  href="#Weight-loss">อ่าน</a>
        		                </div>
        	                </div>
        	            </div>
                      </div> <!-- contain -->
                      <!-- Connect DB -->
                      <?php
                      include "config.php";
                      $objConnect = mysql_connect("$servername","$username","$password") or die("Error Connect to Database");
                      $objDB = mysql_select_db("$dbname");
                      $strSQL = "SELECT * FROM info WHERE Class ='E' ";
                      $objQuery = mysql_query($strSQL) or die ("Error Query [".$strSQL."]")
                      ?>

                        <!-- exercise -->

                        <section id="exercise">
                        <div class="row">
                          <?php
                          while($objResult = mysql_fetch_array($objQuery))
                          { ?>
          	            	<div class="col-sm-3 ">
                              <div class="work wow fadeInUp">
          		                    <img src="<?php echo $objResult["img"];?>">
          		                    <h3><?php echo $objResult["Topic"];?></h3>
          		                    <p> <?php echo $objResult["Detail"];?>
                                  <a href="#<?php echo $objResult["ID"];?>" class="portfolio-link" data-toggle="modal">อ่านต่อ . . .</a></p>
          		                </div>
          	                </div>
                            <!-- Detail -->
                            <div class="portfolio-modal modal fade" id="<?php echo $objResult["ID"];?>" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="close-modal" data-dismiss="modal">
                                            <div class="lr">
                                                <div class="rl">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-sm-10 col-sm-offset-1">
                                                    <div class="modal-body">
                                                        <h3><?php echo $objResult["Topic"];?></h3>
                                                        <img src="<?php echo $objResult["img"];?>">
                                                        <div><?php echo $objResult["Data"];?></div>
                                                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i>ปิด</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end Detail -->
                            <?php } ?>
          	            </div>
                        <a href=""><img src="./assets/img/icon/up.png"></a>
                    </section>


                    <!-- Food -->
                    <?php
                    $strSQL = "SELECT * FROM info WHERE Class ='F'  ";
                    $objQuery = mysql_query($strSQL) or die ("Error Query [".$strSQL."]") ?>

                    <section id="food">
                    <div class="row">
                      <?php
                      while($objResult = mysql_fetch_array($objQuery))
                      { ?>
                      <div class="col-sm-3 ">
                          <div class="work wow fadeInUp">
                              <img src="<?php echo $objResult["img"];?>">
                              <h3><?php echo $objResult["Topic"];?></h3>
                              <p> <?php echo $objResult["Detail"];?>
                              <a href="#<?php echo $objResult["ID"];?>" class="portfolio-link" data-toggle="modal">อ่านต่อ . . .</a></p>
                          </div>
                        </div>
                        <!-- Detail -->
                        <div class="portfolio-modal modal fade" id="<?php echo $objResult["ID"];?>" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="close-modal" data-dismiss="modal">
                                        <div class="lr">
                                            <div class="rl">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-sm-10 col-sm-offset-1">
                                                <div class="modal-body">
                                                    <h3><?php echo $objResult["Topic"];?></h3>
                                                    <img src="<?php echo $objResult["img"];?>">
                                                    <div><?php echo $objResult["Data"];?></div>
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i>ปิด</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end Detail -->
                        <?php } ?>
                    </div>
                    <a href=""><img src="./assets/img/icon/up.png"></a>
                </section>

                      <!-- section general -->
                      <?php
                      $strSQL = "SELECT * FROM info WHERE Class ='G'  ";
                      $objQuery = mysql_query($strSQL) or die ("Error Query [".$strSQL."]") ?>

                      <section id="general">
                      <div class="row">
                        <?php
                        while($objResult = mysql_fetch_array($objQuery))
                        { ?>
                        <div class="col-sm-3 ">
                            <div class="work wow fadeInUp">
                                <img src="<?php echo $objResult["img"];?>">
                                <h3><?php echo $objResult["Topic"];?></h3>
                                <p> <?php echo $objResult["Detail"];?>
                                <a href="#<?php echo $objResult["ID"];?>" class="portfolio-link" data-toggle="modal">อ่านต่อ . . .</a></p>
                            </div>
                          </div>
                          <!-- Detail -->
                          <div class="portfolio-modal modal fade" id="<?php echo $objResult["ID"];?>" tabindex="-1" role="dialog" aria-hidden="true">
                              <div class="modal-dialog">
                                  <div class="modal-content">
                                      <div class="close-modal" data-dismiss="modal">
                                          <div class="lr">
                                              <div class="rl">
                                              </div>
                                          </div>
                                      </div>
                                      <div class="container">
                                          <div class="row">
                                              <div class="col-sm-10 col-sm-offset-1">
                                                  <div class="modal-body">
                                                      <h3><?php echo $objResult["Topic"];?></h3>
                                                      <img src="<?php echo $objResult["img"];?>">
                                                      <div><?php echo $objResult["Data"];?></div>
                                                      <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i>ปิด</button>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <!-- end Detail -->
                          <?php } ?>
                      </div>
                      <a href=""><img src="./assets/img/icon/up.png"></a>
                  </section>

                  <!-- section Weight-loss -->
                  <?php
                  $strSQL = "SELECT * FROM info WHERE Class ='W'  ";
                  $objQuery = mysql_query($strSQL) or die ("Error Query [".$strSQL."]") ?>

                  <section id="Weight-loss">
                  <div class="row">
                    <?php
                    while($objResult = mysql_fetch_array($objQuery))
                    { ?>
                    <div class="col-sm-3 ">
                        <div class="work wow fadeInUp">
                            <img src="<?php echo $objResult["img"];?>">
                            <h3><?php echo $objResult["Topic"];?></h3>
                            <p> <?php echo $objResult["Detail"];?>
                            <a href="#<?php echo $objResult["ID"];?>" class="portfolio-link" data-toggle="modal">อ่านต่อ . . .</a></p>
                        </div>
                      </div>
                      <!-- Detail -->
                      <div class="portfolio-modal modal fade" id="<?php echo $objResult["ID"];?>" tabindex="-1" role="dialog" aria-hidden="true">
                          <div class="modal-dialog">
                              <div class="modal-content">
                                  <div class="close-modal" data-dismiss="modal">
                                      <div class="lr">
                                          <div class="rl">
                                          </div>
                                      </div>
                                  </div>
                                  <div class="container">
                                      <div class="row">
                                          <div class="col-sm-10 col-sm-offset-1">
                                              <div class="modal-body">
                                                  <h3><?php echo $objResult["Topic"];?></h3>
                                                  <img src="<?php echo $objResult["img"];?>">
                                                  <div><?php echo $objResult["Data"];?></div>
                                                  <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i>ปิด</button>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <!-- end Detail -->
                      <?php } ?>
                  </div>
                  <a href=""><img src="./assets/img/icon/up.png"></a>
              </section>



	        </div>
        </div>

        <!-- Footer -->
        <footer>
                <div class="row">
                    <div class="col-sm-7 footer-copyright wow fadeIn">
                        <p></p>
                    </div>

                </div>
            </div>
        </footer>


    <?php
    mysql_close($objConnect);
    ?>




        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/bootstrap-hover-dropdown.min.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/retina-1.1.0.min.js"></script>
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <script src="assets/flexslider/jquery.flexslider-min.js"></script>
        <script src="assets/js/jflickrfeed.min.js"></script>
        <script src="assets/js/masonry.pkgd.min.js"></script>
        <script src="assets/js/jquery.ui.map.min.js"></script>
        <script src="assets/js/scripts.js"></script>

    </body>

</html>
